<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class Contact extends Controller
{
    //
    public function index() {
        return view('contact');
    }

    public function sendMessage(Request $request) {
        dd($request->all());
    }

    public function sendMessageAjax(Request $request) {
        $input = $request->all();
        Mail::send('email', ['email' => $input['email'], 'message' => $input['email']], function($m) {
            $m->from('no-reply@yubimini.com', 'Yubimini');
            $m->to('lathif.is10@gmail.com');
        });

        return response()->json([
            'message' => 'Email Sent',
        ]);
    }
}
